// config: ////////////////////////////////////////////////////////////
// place this file in the same directory with .ino
#define PROTOCOL_TCP

// For AP mode:   choose either AP or STA mode
//#define MODE_AP // phone connects directly to ESP
const char *ssidAP = "GNSS-RTK";  // You will connect your phone to this Access Point
const char *pwAP = "Leafony"; // and this is the password
IPAddress ip(192, 168, 4, 1); // From a console  app, connect to this IP
IPAddress netmask(255, 255, 255, 0);
// You must connect the phone to this AP, then:
// connect -> Internet(TCP) -> 192.168.4.1:8880  for UART0
//                                                           -> 192.168.4.1:8881  for UART1
//                                                           -> 192.168.4.1:8882  for UART2

// For MODE_STA:
#define MODE_STA // ESP tries to connect to a router
const char *ssidSTA = "yourWiFirouter";  // You will connect ESP to this Router
const char *pwSTA = "yourpswd"; // and this is the password

#define BLUETOOTH
const char *ssidBT = "Leafony-Rover";  // You will connect ESP to this Router
const int uartBT = 1;  // connect BT with UART1

// select debug message output devices
const bool useLCD = true;
//const bool useLCD = false;
const bool debug = true;  // assuming to use UART0
//const bool debug = false;

/*************************  COM Port 0 *******************************/
#define UART_BAUD0 115200                    // Baudrate UART0
#define SERIAL_PARAM0 SERIAL_8N1  // Data/Parity/Stop UART0
#define SERIAL0_RXPIN 3                             // receive Pin UART0
#define SERIAL0_TXPIN 1                             // transmit Pin UART0
#define SERIAL0_TCP_PORT 8880          // Wifi Port UART0
/*************************  COM Port 1 *******************************/
#define UART_BAUD1 115200                    // Baudrate UART1
#define SERIAL_PARAM1 SERIAL_8N1 // Data/Parity/Stop UART1
#define SERIAL1_RXPIN 26                          // receive Pin UART1
#define SERIAL1_TXPIN 25                          // transmit Pin UART1
#define SERIAL1_TCP_PORT 8881          // Wifi Port UART1
/*************************  COM Port 2 *******************************/
#define UART_BAUD2 115200                   // Baudrate UART2
#define SERIAL_PARAM2 SERIAL_8N1 // Data/Parity/Stop UART2
#define SERIAL2_RXPIN 16                         // receive Pin UART2
#define SERIAL2_TXPIN 17                         // transmit Pin UART2
#define SERIAL2_TCP_PORT 8882         // Wifi Port UART2

#define bufferSize 1024

//////////////////////////////////////////////////////////////////////////
/*
 *  NTRIP client for Arduino Ver. 1.0.0 
 *  NTRIPClient Sample
 *  Request Source Table (Source Table is basestation list in NTRIP Caster)
 *  Request Reference Data 
 * 
 */
#define USE_NTRIP_CLIENT              //to include the code for NTRIP client
/*
char* hostNtrip = "160.16.134.72";  //CQ-Pub NTRIP Caster
int       portNtrip = 80;                                 //NTRIP_HOST_PORT, typically 2101
char* mntpnt = "CQ-F9P";                    //NTRIP_HOST_MNTPNT;
char* userNtrip   = "guest";                  //NTRIP_CLIENT_USER;
char* passwdNtrip = "guest";             //NTRIP_CLIENT_PSW;
*/
char* hostNtrip = "192.168.1.110";  //test in intranet (@ my home)
int       portNtrip = 2101;                           //NTRIP_HOST_PORT, typically 2101
char* mntpnt = "F9PS1";                       //NTRIP_HOST_MNTPNT;
char* userNtrip   = "ESP32NODE";   //NTRIP_CLIENT_USER;
char* passwdNtrip = "leafony";         //NTRIP_CLIENT_PSW;
//

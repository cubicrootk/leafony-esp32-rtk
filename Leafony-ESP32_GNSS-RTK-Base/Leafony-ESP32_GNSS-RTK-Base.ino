// Leafony ESP32 GNSS-RTK Base Station
//     Leaf configuration
//       (1) AP02A ESP32 CPU
//       (2) AI04 LCD 
//       (3) AX01 Shield to connect from ESP32 UART1 to  F9P-UART1 on SimpleRTK2B
//    (note that ESP32 USB program port conflicts with F9P-UART1 via the Sheild Rx/Tx)
//
//    (c) 2020  cubicroot.com //Trillion-Node Study Group
//    Released under the MIT license
//    https://opensource.org/licenses/MIT
//
//    Rev.01:  2020/08/27  First release
//----------
// Cited "ESP32-Serial-Bridge" from https://github.com/AlphaLima/ESP32-Serial-Brige
// but it was made simplified for GNSS reciever usage.
// Disclaimer: Don't use  for life support systems or any other situations
// where system failure may affect user or environmental safety.
//----------
// Cited "NTRIP-server" from https://github.com/GLAY-AK2
//----------
// Cited AI04 LCD driver from https://raw.githubusercontent.com/Leafony/
//                   Sample-Sketches/master/LCD-SW_test/LCD-SW_test.ino
//----------
// Tested with  u-Blox ZED-F9P on the "simpleRTK2B v1"
//      https://www.ardusimple.com/simplertk2b/
//=====================================================================

#pragma GCC optimize("O3")

#include "GNSS-RTK_config.h"
#include <esp_wifi.h>
#include <WiFi.h>

#ifdef BLUETOOTH
#include <BluetoothSerial.h>
BluetoothSerial SerialBT; 
bool client_BT = false;
#endif

HardwareSerial Serial_1(1);
HardwareSerial Serial_2(2);

#ifdef PROTOCOL_TCP
#include <WiFiClient.h>
WiFiServer server_0(SERIAL0_TCP_PORT);
WiFiServer server_1(SERIAL1_TCP_PORT);
WiFiServer server_2(SERIAL2_TCP_PORT);
WiFiClient client_1;    // ? 初期化？
WiFiClient client_2;
#endif

uint8_t buf1[bufferSize];
uint16_t buf1Size=0;
uint8_t buf2[bufferSize];
uint16_t buf2Size=0;
uint8_t bufBT[bufferSize];
uint16_t bufBTsize=0;

#ifdef USE_NTRIP_SERVER
#include "NTRIPServer.h"
NTRIPServer ntrip_s;
#endif
const int bufNtripMax = 512;
char bufNtrip[bufNtripMax];
int bufNTsize = 0;
bool ntripUart1 = false;

// Include the display library for "AI04"
// For a connection via I2C using the Arduino Wire include:
 #include <Wire.h>
 #include <ST7032.h>
 ST7032 lcd; // Initialize the Character display 
 char buf[10];
 String ipadrs;
 #define I2C_EXPANDER_ADDR   0x1A
 
String ipToString(uint32_t ip){
    String result = "";
    result += String((ip & 0xFF), 10);
    result += ".";
    result += String((ip & 0xFF00) >> 8, 10);
    result += ".";
    result += String((ip & 0xFF0000) >> 16, 10);
    result += ".";
    result += String((ip & 0xFF000000) >> 24, 10);
    return result;
}

void setup() {
// Initialising the LCD character display "AI04"
  Wire.begin();
// IO Expander Initialization, then LCD Power on
  i2c_write_byte(I2C_EXPANDER_ADDR, 0x03, 0xFE);
  i2c_write_byte(I2C_EXPANDER_ADDR, 0x01, 0x01);
  lcd.begin(8, 2);   //LCD Initialize
  lcd.setContrast(30);
  lcd.clear();
  lcd.setCursor(0, 0);
  lcd.print("WiFiUART");
  delay(500);

// start serial ports     
  Serial.begin(UART_BAUD0, SERIAL_PARAM0, SERIAL0_RXPIN, SERIAL0_TXPIN);
  Serial_1.begin(UART_BAUD1, SERIAL_PARAM1, SERIAL1_RXPIN, SERIAL1_TXPIN);
  Serial_2.begin(UART_BAUD2, SERIAL_PARAM2, SERIAL2_RXPIN, SERIAL2_TXPIN);
  if(debug) {
    Serial.println(" "); 
    Serial.println("WiFi Serial Bridge"); 
  }

  #ifdef MODE_AP 
  if(useLCD) {lcd.setCursor(0, 1); lcd.print("AP mode ");}
  //AP mode (phone connects directly to ESP) (no router)
  WiFi.mode(WIFI_AP);
  WiFi.softAP(ssidAP, pwAP); // configure ssid and password for softAP
  delay(2000); // VERY IMPORTANT
  WiFi.softAPConfig(ip, ip, netmask); // configure ip address for softAP
  #endif

  #ifdef MODE_STA
  if(useLCD) {lcd.setCursor(0, 0); lcd.print("STA mode");}
  // STATION mode (ESP connects to router and gets an IP)
  // Assuming phone is also connected to that router
  WiFi.mode(WIFI_STA);
  WiFi.begin(ssidSTA, pwSTA);  

 // Show progress of connection
 if(useLCD) {lcd.blink();}
  for (int i = 0; i < 16; i++) {
    if(debug) Serial.print(".");  
    if(useLCD) {lcd.setCursor((i % 8), 0);}
    delay(250);
    if (WiFi.status() == WL_CONNECTED) break;
  }
   if(useLCD) {lcd.noBlink();}
  if (WiFi.status() == WL_CONNECTED) {
  // Show the IP address as String
    if(debug) {
      Serial.print("WiFi connected, IP address: ");   
      Serial.println(WiFi.localIP());
    }
    if(useLCD) {
      ipadrs = ipToString(WiFi.localIP());
      lcd.setCursor(0, 1);
      lcd.print(ipadrs.substring(ipadrs.length()-8));
    }
  }
  else{
    if(useLCD) {lcd.setCursor(0, 1);  lcd.print("not cnct");}
  }
#endif //MODE_STA
  
#ifdef BLUETOOTH
  if(debug) Serial.println("Open Bluetooth Server");  
  SerialBT.begin(ssidBT);    // Bluetooth device name
#endif

#ifdef PROTOCOL_TCP
  server_0.begin(); // start TCP server 
  server_0.setNoDelay(true);
  server_1.begin(); // start TCP server 
  server_1.setNoDelay(true);
  server_2.begin(); // start TCP server   
  server_2.setNoDelay(true); 
  if(debug) Serial.println("Started TCP Servers"); 
  if(useLCD) {lcd.setCursor(4, 0);  lcd.print("TCP "); }
#endif //PROTOCOL_TCP

#ifdef USE_NTRIP_SERVER
  if(useLCD) {lcd.setCursor(4, 0);  lcd.print("ssMP");}
  if(debug) {Serial.println("Subscribing MountPoint of NTRIP");}
  if (!ntrip_s.subStation(hostNtrip, portNtrip, mntpnt, pswNtrip, srcSTR)) {
    delay(10000);
    ESP.restart();
  }
    if(debug)  Serial.println("Subscribing MountPoint is OK");
    if(useLCD) { lcd.setCursor(4, 0); lcd.print("ntMP"); }
#endif // USE_NTRIP_SERVER

 esp_err_t esp_wifi_set_max_tx_power(50);  //lower WiFi Power
 
}  // end of setup()

void loop() {
  
#ifdef BLUETOOTH
  // receive from Bluetooth:
  if(SerialBT.hasClient()) {
    if (!client_BT) {
      if(debug) Serial.println("New BT client");
      if(useLCD) { lcd.setCursor(4, 0); lcd.print("clBT "); }
      client_BT = true;
    }
    while(SerialBT.available())
    {
      bufBT[bufBTsize] = SerialBT.read(); // read char from client (a console app)
      if(bufBTsize <bufferSize-1) bufBTsize++;
    }          
//  if(debug)  Serial.write(bufBT,bufBTsize); 
     if (uartBT == 1) {
       Serial_1.write(bufBT,bufBTsize); // now send to UART1:
     }
     if (uartBT == 2) {
      Serial_2.write(bufBT,bufBTsize); // now send to UART2:
     }
     bufBTsize = 0;
  }  
#endif //BLUETOOTH 

#ifdef PROTOCOL_TCP
// wait for client
 if (server_1.hasClient()){
    if (!client_1){
      if(useLCD) {lcd.setCursor(4, 0); lcd.print("Clt1");}
      if(debug) Serial.println("New TCP client for UART1");
    }  
    client_1 = server_1.available();  
 }

 if (server_2.hasClient()) {  
    if(!client_2) {
      if(useLCD) {lcd.setCursor(4, 0); lcd.print("Clt2");}
      if(debug) Serial.println("New TCP client for UART2");
    }
    client_2 = server_2.available();
}

 if (client_1) { 
    while(client_1.available()) {
      buf1[buf1Size] = client_1.read(); // read char from TCP-client (a console app)
      if(buf1Size<bufferSize-1) buf1Size++;
    } 
    Serial_1.write(buf1, buf1Size); // now send to UART:
    buf1Size = 0;

   while(Serial_1.available()) {     
      buf2[buf2Size] = Serial_1.read(); // read char from UART1
      if(buf2Size<bufferSize-1) buf2Size++;
   }
             
    client_1.write(buf2, buf2Size); // now send to WiFi:
#ifdef BLUETOOTH
    if (uartBT == 1) {
       if(SerialBT.hasClient()) SerialBT.write(buf2, buf2Size);   // now send to Bluetooth:}
    }
#endif  //BLUETOOTH  
    buf2Size = 0;      
 }   
 
  if (client_2) { 
    while(client_2.available()) {
      buf1[buf1Size] = client_2.read(); // read char from TCP-client (a console app)
      if(buf1Size<bufferSize-1) buf1Size++;
    } 
    Serial_2.write(buf1, buf1Size); // now send to UART:
    buf1Size = 0;

   while(Serial_2.available()) {     
       buf2[buf2Size] = Serial_2.read(); // read char from UART2
       if(buf2Size<bufferSize-1) buf2Size++;
    }
    client_2.write(buf2, buf2Size); // now send to WiFi:
#ifdef BLUETOOTH
    if (uartBT == 2) {
      if(SerialBT.hasClient())  SerialBT.write(buf2, buf2Size);   // now send to Bluetooth:
    }  
#endif //BLUETOOTH
    buf2Size = 0;      
 }
#endif //PROTOCOL_TCP
  
#ifdef USE_NTRIP_SERVER
if (ntrip_s.connected()) {
    while (Serial_1.available()) {
      bufNTsize = 0;
      while (Serial_1.available()) {
        bufNtrip[bufNTsize] = Serial_1.read();
        bufNTsize++;
        if (bufNTsize > (bufNtripMax - 1 ))break;
      } //buffering
      ntrip_s.write((uint8_t*)bufNtrip, bufNTsize);
    }
    if  (ntripUart1  == false) {
       ntripUart1 = true;    
       if(debug) Serial.println("entered the loop of UART1 read toward NTRIP"); 
       if(useLCD) {lcd.setCursor(0, 1); lcd.print("NTs1");}
    }
 }
 else {
    ntrip_s.stop();
    if(debug) Serial.println("Try to reconnect and subscribing MountPoint again");
    if(useLCD) {lcd.setCursor(0, 0); lcd.print("NTrecnct");}
    if (!ntrip_s.subStation(hostNtrip, portNtrip, mntpnt, pswNtrip, srcSTR)) {
      delay(100);
    }
    else {
      if(debug) Serial.println("Reconnected MountPoint is OK");
      delay(10);
    }
 }
 delay(10);  //server cycle
#endif // USE_NTRIP_SERVER
 
}  // end of loop()
/**********************************************
* I2C スレーブデバイスに1バイト書き込む
**********************************************/
void i2c_write_byte(int device_address, int reg_address, int write_data){
  Wire.beginTransmission(device_address);
  Wire.write(reg_address);
  Wire.write(write_data);
  Wire.endTransmission();
}
/**********************************************
* I2C スレーブデバイスから1バイト読み込む
**********************************************/
unsigned char i2c_read_byte(int device_address, int reg_address){

  int read_data = 0;

  Wire.beginTransmission(device_address);
  Wire.write(reg_address);
  Wire.endTransmission(false);

  Wire.requestFrom(device_address, 1); 
  read_data = Wire.read();

  return read_data;
}

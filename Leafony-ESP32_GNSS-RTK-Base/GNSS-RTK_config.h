// config: ////////////////////////////////////////////////////////////
// place this file in the same directory with .ino 
#define PROTOCOL_TCP

// For AP mode:  choose either AP or STA mode
//#define MODE_AP // phone connects directly to ESP
const char *ssidAP = "GNSS-RTK";  // You will connect your phone to this Access Point
const char *pwAP = "Leafony"; // and this is the password
IPAddress ip(192, 168, 4, 1); // From a console  app, connect to this IP
IPAddress netmask(255, 255, 255, 0);
// You must connect the phone to this AP, then:
//connect -> Internet(TCP) -> 192.168.4.1:8880  for UART0
//                                                          -> 192.168.4.1:8881  for UART1
//                                                          -> 192.168.4.1:8882  for UART2

// For MODE_STA:
#define MODE_STA // ESP tries to connect to a router
const char *ssidSTA = "yourWiFirouter";  // You will connect ESP to this Router
const char *pwSTA = "yourpswd"; // and this is the password

#define BLUETOOTH
const char *ssidBT = "Leafony-Base";  // You will connect ESP to this Router
const int uartBT = 1;  // connect BT with UART 1

bool useLCD = true;
//bool useLCD = false;
bool debug = true;  // assuming to use UART0
//bool debug = false;

/*************************  COM Port 0 *******************************/
#define UART_BAUD0 115200                   // Baudrate UART0
#define SERIAL_PARAM0 SERIAL_8N1 // Data/Parity/Stop UART0
#define SERIAL0_RXPIN 26                         // receive Pin UART0    orignally 03
#define SERIAL0_TXPIN 25                         // transmit Pin UART0 orignally 01
#define SERIAL0_TCP_PORT 8880         // Wifi Port UART0
/*************************  COM Port 1 *******************************/
#define UART_BAUD1 115200                   // Baudrate UART1
#define SERIAL_PARAM1 SERIAL_8N1 // Data/Parity/Stop UART1
#define SERIAL1_RXPIN 3                            // receive Pin UART1    orignally 26
#define SERIAL1_TXPIN 1                            // transmit Pin UART1 orignally 25
#define SERIAL1_TCP_PORT 8881         // Wifi Port UART1
/*************************  COM Port 2 *******************************/
#define UART_BAUD2 115200                   // Baudrate UART2
#define SERIAL_PARAM2 SERIAL_8N1// Data/Parity/Stop UART2
#define SERIAL2_RXPIN 16                         // receive Pin UART2    orignally 15
#define SERIAL2_TXPIN 17                         // transmit Pin UART2   orignally 4
#define SERIAL2_TCP_PORT 8882         // Wifi Port UART2

#define bufferSize 1024

//////////////////////////////////////////////////////////////////////////
/*
 *  NTRIP Server for Arduino Ver. 1.0.0 
 *  Sample (use rtk2go.com(ntrip softwere test server))
 */
//char* hostNtrip = "rtk2go.com";    //ntrip caster host address 
char* hostNtrip = "192.168.1.110";    //intranet (@ my home)
int portNtrip = 2101;                               //port 2101 is default port of NTRIP caster
//char* mntpnt = "mountpoint";       //must change this string
char* mntpnt = "F9PS1";       //must change this string
char* pswNtrip   = "leafony";     //enter password of ntrip caster for subscribing
char* srcSTR   = "";    //source table infomation string reference software.rtcm-ntrip.org/wiki
//                     : NTRIP sourcetable contents (https://software.rtcm-ntrip.org/wiki/Sourcetable)
#define USE_NTRIP_SERVER              //to use the code for NTRIP server
//

# Leafony ESP32 Kit による GNSS-RTK の実装例

GPSを代表とする衛星測位システム（GNSS）[^1]の精度を高める手法の一つであるネットワーク型RTK方式[^2]を、 Leafony[^3] ESP32キットを2台使用して実装しました。
GNSS受信機はArduSimple社(LSIはu-Blox社のZED-F9P)のボードを2種類使用。
WiFiのステーションモードとシリアルブリッジを実装し、WiFiのアクセスポイント経由で(衛星測位)の補正データを基準局から移動局に送ります。
[^1]:Global Navitaion Sattelite Systems [GNSSとは](images/GNSS_overview.png)
[^2]:Real Time Kinetics [RTK方式の概要](images/GNSS_and_RTk.png)
[^3]:Leafony Systems Co. Ltd [トリリオンノード研究会にて開発](https://leafony.com/)

## 使用機材

* [Leafony ESP32 Wi-Fi Kit](https://shop.leafony.com/collections/products/products/esp32-wi-fi-kit-2) : AP02 x 2pcs, AX07
* [Leafony Extension Kit](https://shop.leafony.com/collections/products/products/extension-kit) : AI04 x 2pcs, AX01
* [Leafony Basic Kit](https://docs.leafony.com/docs/products/basic2/) : AX02
* [simpleRTK2B](https://www.ardusimple.com/simplertk2b/): ver.1、基準局に使用 (GNSSアンテナは別途必要)
* [simpleRTK2Blite](https://www.ardusimple.com/simplertk2blite/) : 移動局に使用（GNSSアンテナは別途必要）
* 電源：モバイルバッテリーパックからUSBケーブルにて給電した。
* その他：ユニバーサル基板、ピンヘッダ、ジャンパー線、ボルトナット、三脚等々

## ソフトウェア

* Arduino IDE (使用したLibraryについてはソースコードに記載）
* [u-Center](https://www.u-blox.com/en/product/u-center) : ZED-F9Pの設定に使用（Windowsのみ）
* コンソールアプリ（PuTTYとか）
* Androidスマホ（Bluetooth接続チェックに使用）

## 参考文献

* [センチメートルGPS測位 F9P RTKキット・マニュアル](image/../images/ToraGi-list.png)  (CQ出版社)
* [GNSS測位入門からRTKLIBの活用まで](http://gpspp.sakura.ne.jp/paper2005/IPNTJ_Seminar_2015_1.pdf) (測位航法学会)
* [GEONET（GNSS連続観測システム）とは](https://www.gsi.go.jp/eiseisokuchi/eiseisokuchi41012.html) (国土地理院)

## 実装の詳細

__A. simpleRTK2B と simpleRTK2Blite について__

* simpleRTK2BとsimpleRTK2BliteにはXbeeのコネクタがあり、各々にZigBeeを装着してトランスペアレントモードで接続すれば（Leafonyを使うまでもなく）GNSS-RTKとして動作する
* 今回ESP32キットを使う理由は
  * [ネットワーク型のRTKを構成する事と](images/RTk_methods.png)、
  * WiFiシリアル・ブリッジ機能で u-Center をデバック用途に接続する事

__B. 基準局の構成__

* simpleRTK2Bは、Arudinoシールドとして利用できるので拡張キットのシールドと組み合わせた
* simpleRTK2BのUARTはarduino端子のRX/TXを使うので、ESP32リーフのUSBからのプログラミングポートと競合する。面倒でもsimpleRTK2Bボードを外してフラッシュへの書き込みをした
* 上記の理由から、UART0の端子を付け替えて、UART1をRX/DX端子に設定した
* LeafonyのArudinoシールドのIOREF端子がオープンである為、simpleRTK2Bボード側で3.3v端子にジャンパー線で接続必要
* simpelRTK2BボードのXbee側のUSBポートに給電し、本ボードの3.3V電源をLeafony側の3.3V電源に”借用”した

__C. 移動局の構成__

* simpleRTK2BliteとESP32リーフとの接続についてはユニバーサル基板で自作
* 29ピンリーフ(AX02)をピンヘッダーにてユニバーサル基板に接続
* BacktoBackリーフ(AX07)にてESP32、LCDリーフを接続
* Xbee変換基板をユニバーサル基板に接続
* XbeeソケットにsimpleRTK2Bliteを搭載
* simpleRTKBliteのUART1を ESP32のUART1に接続
* ESP32リーフのUSBポートから全体に給電

__D. 衛星測位の補正データ__

* 補正データの形式には幾つか種類があるが、今回はRTCM3を利用した
* 補正データをインターネット経由で配信する為にNTRIP方式がある
  * [NTRIPの参考資料](http://www.denshi.e.kaiyodai.ac.jp/gnss_tutor/pdf/Basestation2.pdf)
* SNIPという商用サービスがあり機能限定の無料版を試行した
  * [SNIPの参考資料](http://www.rtk2go.com/how-it-works/)

__E. アクションアイテム__

* [x] ESP32の WiFi Station Mode にPCのPuTTYから接続
* [x] 同上でPCの u-Center から F9P に接続
* [x] ESP32の Bluetooth mode にスマホのアプリ(Serial Bluetooth Terminal)から接続
* [x] 基準局側のESP32の WiFi Staion Mode から イントラネット内のSNIPアプリへPush-In
* [x] 移動局側のESP32の WiFi Station Mode から、インターネットでCQ出版社のテストサーバーに接続
* [x] 移動局側のESP32の WiFi Station Mode から イントラネット内のSNIPアプリに接続できた
* [ ] 上記で、数分で接続が切れる課題
* [x] 上記の基準局から移動局にSNIP経由でRTCM3を送信できた
* [ ] 上記で、CRCエラーが多発する場合がある課題
* [ ] 基地局側の3.3Vの給電の電源容量不足の懸念
* [ ] 測位精度検証は未着手
* [ ] SDカードにLogを取りたい

__F. 基準局のハードウェア組み立ての詳細__

![シールドの組み立ての様子](images/Leafony-ESP32_on_simpleRTK2B.png)

__G. 移動局のハードウェア組み立ての詳細__

![ユニバーサル基板、Xbee変換基板と29ピンリーフ（AX02)で自作基板](images/Leafony-ESP32_and_simpleRTK2Blite.png)
  
__H. 基準局と移動局を利用したネットワーク型RTKの実装例__  

![実装例](images/Network-RTK_by_Leafony-ESP32.png)

#### 本資料について

本資料は[CC BY-SA 4.0（クリエイティブ・コモンズ 表示 4.0 継承 国際 ライセンス）](http://creativecommons.org/licenses/by-sa/4.0/deed.ja)の元で公開します。

![クリエイティブ・コモンズ・ライセンス](https://i.creativecommons.org/l/by-sa/4.0/88x31.png)　2020©計画工学研究所
